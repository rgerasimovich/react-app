var gulp            = require('gulp');
var path            = require('path');
var autoprefixer    = require('gulp-autoprefixer');
var babel           = require('gulp-babel');
var browserSync     = require('browser-sync');
var concat          = require('gulp-concat');
var eslint          = require('gulp-eslint');
var filter          = require('gulp-filter');
var newer           = require('gulp-newer');
var notify          = require('gulp-notify');
var plumber         = require('gulp-plumber');
var reload          = browserSync.reload;
var sass            = require('gulp-sass');
var sourcemaps      = require('gulp-sourcemaps');
var browserify      = require('browserify');
var gutil           = require('gulp-util');
var source          = require('vinyl-source-stream');
var $               = require('gulp-load-plugins')();
var clean           = require('gulp-clean');
var cssScss         = require('gulp-css-scss');
var replace         = require('gulp-replace');

var errorHandler = function(title) {
    'use strict';

    return function(err) {
        gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
        this.emit('end');
    };
};

var buildStyles = function() {
    var sassOptions = {
        style: 'expanded'
    };

    var injectFiles = gulp.src(['source/**/*.scss', 'bower_components_scss/**/*.min.scss', '!source/all.scss'], { read: false });

    var injectOptions = {
        transform: function(filePath) {
            return '@import "' + filePath + '";';
        },
        starttag: '// injector',
        endtag: '// endinjector',
        addRootSlash: false
    };


    return gulp.src(['source/all.scss'])
        .pipe($.inject(injectFiles, injectOptions))
        .pipe($.sourcemaps.init())
        .pipe($.sass(sassOptions)).on('error', errorHandler('Sass'))
        .pipe($.autoprefixer()).on('error', errorHandler('Autoprefixer'))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(path.join('dist')));
};

var proxyMiddleware = require('http-proxy-middleware');

var browserSyncInit = function(baseDir, browser) {
    browser = browser === undefined ? 'default' : browser;

    var routes = null;
    //if(baseDir === conf.paths.src || (util.isArray(baseDir) && baseDir.indexOf(conf.paths.src) !== -1)) {
    routes = {
        '/bower_components': 'bower_components'
    };
    //}

    var server = {
        baseDir: baseDir,
        routes: routes
    };

    var target = 'http://localhost';

    server.middleware = proxyMiddleware('/testApi', {target: target, onError: onProxyError,ws:true});

    // баг на macOs, корректный ответ от прокси интерпритируется как ошибочный,
    // поэтому мы сами обрабатываем ошибку
    function onProxyError(err, req, res) {
        var host = (req.headers && req.headers.host);
        console.error('Error occured while trying to proxy to: ' + host + req.url + "/n" + err);
        res.end();
    }

    browserSync.instance = browserSync.init({
        startPath: '/',
        server: server,
        browser: browser
    });
}

gulp.task('bower-scss', () => {
    return gulp.src(['bower_components/**/*.min.css'])
        .pipe(cssScss())
        .pipe(replace('screen\\0','screen'))
        .pipe(gulp.dest(path.join('bower_components_scss')));
});

gulp.task('js', function() {
    return browserify({
        entries: './source/app.jsx',
        extensions: ['.jsx'],
        debug: true
    })
        .transform('babelify', {
            presets: ['es2015', 'stage-0', 'react'],
            plugins: ['transform-class-properties']
        })
        .bundle()
        .on('error', function(err){
            gutil.log(gutil.colors.red.bold('[browserify error]'));
            gutil.log(err.message);
            this.emit('end');
        })
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('sass', function() {
    return buildStyles();
});

gulp.task('styles-reload', ['sass'], function() {
    return buildStyles()
        .pipe(browserSync.stream());
});

gulp.task('js-reload', ['js'], function() {
    browserSync.reload();
});

gulp.task('watch', function() {
    gulp.watch('source/**/*.{js,jsx}', ['js-reload']);
    gulp.watch('source/**/*.scss', ['styles-reload']);
});

gulp.task('browsersync', function () {
    browserSyncInit('./');
});

gulp.task('build', ['bower-scss', 'sass', 'js']);

gulp.task('serve', ['build', 'browsersync', 'watch']);