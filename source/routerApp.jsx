import React, { Component } from 'react';
import {Router, Route, IndexRedirect} from 'react-router';
import { Provider } from 'react-redux';
import {createHashHistory} from 'history';
import Chats from './Chats/chatsContainer';
import Chat from './Chat/chatContainer';
import {IndexPage} from './components/IndexPage';
import configureStore from './store/configureStore'
const store = configureStore();

export class RouterApp extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Provider store={store}>
                <Router history={createHashHistory({queryKey: false})}>
                    <Route path="/" component={IndexPage}>
                        <IndexRedirect to='chats' />
                        <Route path="chats" component={Chats}/>
                        <Route path="chat/:userId" component={Chat}/>
                    </Route>
                </Router>
            </Provider>
        )
    }
}