import * as React from 'react';

export class IndexPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="main-wrap">
                <div className="chats-wrap" data-flex data-layout="column" data-layout-align="stretch stretch">
                    {this.props.children}
                </div>
            </div>
        );
    }
}