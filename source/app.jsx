import React from 'react';
import { render } from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';

import * as constants from './constants';
import { AppConfig } from './appConfig';

moment.locale('ru');
injectTapEventPlugin();

render(
    <AppConfig />,
    document.getElementById(constants.APP_CONTAINER_ID)
);