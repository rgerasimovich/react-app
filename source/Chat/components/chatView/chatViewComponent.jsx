import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Router, Link} from 'react-router'
import {Message} from '../message/messageComponent';
import {InputMessage} from '../inputMessage/inputMessageComponent';

export class ChatView extends React.Component {
    constructor(props) {
        super(props);
        this.props.getAllMessage(this.props.chat.user.id);
    }

    scrollChatToBottom() {
        let height = 0;
        let interval = window.setInterval(() => {
            if ($(".dialog-wrap").length) {
                window.clearInterval(interval);
                let messages = $(".dialog-wrap")[0].children;
                for (let i = 0; i < messages.length; i++) {
                    height += $(messages[i]).outerHeight();
                }
                $($(".dialog-wrap")[0]).scrollTop(height);
            }
        }, 15);
    }

    render() {
        const { chat, file, loading, sendMessage, setFile, removeFile } = this.props;
        this.scrollChatToBottom();
        return (
            <div className="chat-view-container">
                {
                    loading ?
                        <div>Загрузка...</div>
                    :
                        <div className="chat-view">
                            <div className="user-info">
                                <div className="user-info__name">{chat.user.name}</div>
                                <div data-flex></div>
                                <Link to={'/chats'}><div className="user-info__back">Все диалоги</div></Link>
                            </div>
                            <div className="dialog-wrap" data-flex>
                                {
                                    chat.message.map((entry, index) =>
                                        <Message message={entry} key={index}/>
                                    )
                                }
                            </div>
                            <InputMessage sendMessage={sendMessage} file={file} idUser={chat.user.id} setFile={setFile} removeFile={removeFile} loading={loading} />
                        </div>
                }
            </div>
        );
    }
}