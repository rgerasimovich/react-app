import * as React from 'react';
import * as ReactDOM from 'react-dom';

export class Message extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { message } = this.props;
        let dateMessage = moment(message.date);

        return (
            <div className={message.type === 'FROM' ? 'message-container message-container--from' : 'message-container'}>
                {
                    message.type === 'FROM' ?
                        <div className="message-container__date">{dateMessage.format('DD.MM.YYYY в HH:mm:ss')}</div>
                        :
                        <div></div>
                }
                <div className="message-entity">
                    {message.text}
                    {
                        message.fileBody ?
                            <a href={message.fileBody} target="_blank"><div className="message-entity__file-wrap">{message.fileName} ({message.fileSize})</div></a>
                            :
                            <div></div>
                    }
                </div>
                {
                    message.type === 'TO' ?
                        <div className="message-container__date">{dateMessage.format('DD.MM.YYYY в HH:mm:ss')}</div>
                        :
                        <div></div>
                }
            </div>
        );
    }
}