import * as React from 'react';
import * as ReactDOM from 'react-dom';

export class InputMessage extends React.Component {
    constructor(props) {
        super(props);
    }

    sendMessage(e) {
        e.preventDefault();
        let message = ReactDOM.findDOMNode(this.refs.message).value;
        if (!message && !this.fileBody) {
            return;
        }
        this.props.sendMessage(this.props.idUser, message);
        $("#add-file")[0].outerHTML = $("#add-file")[0].outerHTML;
        this.props.removeFile();
        ReactDOM.findDOMNode(this.refs.message).value = '';
    }

    removeFile() {
        $("#add-file")[0].outerHTML = $("#add-file")[0].outerHTML;
        this.props.removeFile();
    }

    addFile(e) {
        e.preventDefault();
        $("#add-file").trigger('click');
        $('#add-file').unbind('change');
        $("#add-file").change((e) => {
            this.readFile($("#add-file"));
        });
    }

    readFile(input) {
        if (input[0].files && input[0].files[0]) {
            let reader = new FileReader();

            reader.onload = (e) => {
                this.props.setFile(input[0].files[0].name, input[0].files[0].size + ' Б.', e.target.result);
            };

            reader.readAsDataURL(input[0].files[0]);
        }
    }

    render() {
        const { loading, file } = this.props;

        return (
            <form onSubmit={::this.sendMessage}>
                <div className="input-wrap">
                    <div className="input-wrap__file-button" onClick={::this.addFile}>
                        {
                            file ?
                                <div>{file.fileName}</div>
                                :
                                <div>файл</div>
                        }
                    </div>
                    {
                        file ?
                            <div className="input-wrap__file-remove" onClick={::this.removeFile}>X</div>
                            :
                            <div></div>
                    }
                    <input type="file" className="add-file" id="add-file"/>
                    <input
                        type='text'
                        className='input-wrap__message'
                        placeholder='Введите сообщение'
                        ref='message'
                        disabled={loading}
                    />
                    <div className="input-wrap__send-button" onClick={::this.sendMessage}>отправить</div>
                </div>
            </form>
        );
    }
}