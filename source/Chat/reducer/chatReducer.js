import * as ConstantChats from '../constants/chatConstants';

const initialState = {
    loading: true,
    fistLoad: false,
    fileName: null,
    fileSize: null,
    fileBody: null
};

export default function chat(state = initialState, action) {

    switch (action.type) {

        case ConstantChats.SET_FILE:
            return { ...state, fileName: action.fileName, fileSize: action.fileSize, fileBody: action.fileBody };

        case ConstantChats.REMOVE_FILE:
            return { ...state, fileName: null, fileSize: null, fileBody: null };

        default:
            return state;
    }

}