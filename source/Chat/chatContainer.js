import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as chatActions from './actions/chatActions'
import * as chatsActions from '../Chats/actions/chatsActions'
import {ChatView} from './components/chatView/chatViewComponent';

class Chat extends Component {
    constructor(props) {
        super(props);
        this.props.chatsActions.getChats();
    }

    render() {
        const { chats, chat } = this.props;
        const { setFile, removeFile } = this.props.chatActions;
        const { getAllMessage, sendMessage } = this.props.chatsActions;
        const userId = this.props.params.userId;
        let chatItem = null;
        chats.chats.forEach(item => {
           if (item.user.id === parseInt(userId)) {
               chatItem = item;
           }
        });

        const file = chat.fileBody ? {
                fileName: chat.fileName,
                fileSize: chat.fileSize,
                fileBody: chat.fileBody
            } : null;

        return (
            <div className="chat-view-container">
                {
                    chats.chats.length ?
                        <ChatView chat={chats.chats.length ? chatItem : []} file={file} getAllMessage={getAllMessage} setFile={setFile} removeFile={removeFile} sendMessage={sendMessage} loading={chats.loading} />
                        :
                        <div>Загрузка...</div>
                }
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        chats: state.chats,
        chat: state.chat
    }
}

function mapDispatchToProps(dispatch) {
    return {
        chatActions: bindActionCreators(chatActions, dispatch),
        chatsActions: bindActionCreators(chatsActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)