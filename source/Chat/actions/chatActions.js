import * as ConstantChats from '../constants/chatConstants'

export function setFile(fileName, fileSize, fileBody) {

    return (dispatch) => {

        dispatch({
            type: ConstantChats.SET_FILE,
            fileName: fileName,
            fileSize: fileSize,
            fileBody: fileBody
        });
    }
}

export function removeFile() {

    return (dispatch) => {

        dispatch({
            type: ConstantChats.REMOVE_FILE
        });
    }
}