import { combineReducers } from 'redux'
import chats from '../Chats/reducer/chatsReducer'
import chat from '../Chat/reducer/chatReducer'

export default combineReducers({
    chats, chat
})