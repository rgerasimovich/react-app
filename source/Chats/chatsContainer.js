import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as chatsActions from './actions/chatsActions'
import {ChatsComponent} from './components/chats/chatsComponent';

class Chats extends Component {
    render() {
        const { chats } = this.props;
        const { getChats } = this.props.chatsActions;

        return (
             <ChatsComponent chats={chats.chats} getChats={getChats} loading={chats.loading} />
        )
    }
}

function mapStateToProps(state) {
    return {
        chats: state.chats
    }
}

function mapDispatchToProps(dispatch) {
    return {
        chatsActions: bindActionCreators(chatsActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chats)