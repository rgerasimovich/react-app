import * as ConstantChats from '../constants/chatsConstants';

const initialState = {
    chats: [],
    loading: true
};

export default function chats(state = initialState, action) {

    switch (action.type) {
        case ConstantChats.GET_CHATS_REQUEST:
            return { ...state, loading: true };

        case ConstantChats.GET_CHATS_SUCCESS:
            return { ...state, chats: action.chats ? action.chats : state.chats, loading: false, fistLoad: true };

        case ConstantChats.GET_CHAT_REQUEST:
            return { ...state, loading: true };

        case ConstantChats.GET_CHAT_SUCCESS:

            if (!action.chat) {
                return { ...state, loading: false }
            }
            let findIndex = -1;
            let newArray = state.chats;
            newArray.forEach((item, index) => {
                if (item.user.id === parseInt(action.idUser)) {
                    findIndex = index;
                }
            });
            newArray[findIndex].message = action.chat;
            newArray[findIndex].loadAllMessage = true;

            return { ...state, chats: newArray, loading: false };

        case ConstantChats.SEND_MESSAGE_REQUEST:
            return { ...state, loading: true };

        case ConstantChats.SEND_MESSAGE_SUCCESS:

            let indexElement = -1;
            let arrayChats = state.chats;
            arrayChats.forEach((item, index) => {
                if (item.user.id === parseInt(action.idUser)) {
                    indexElement = index;
                }
            });
            arrayChats[indexElement].message.push({
                text: action.message,
                date: new Date().getTime(),
                type: "FROM",
                fileName: state.fileName,
                fileSize: state.fileSize,
                fileBody: state.fileBody
            });

            return { ...state, chats: arrayChats, loading: false };

        case ConstantChats.SEND_ANSWER_SUCCESS:

            let i = -1;
            let array = state.chats;
            array.forEach((item, index) => {
                if (item.user.id === parseInt(action.idUser)) {
                    i = index;
                }
            });
            array[i].message.push({
                text: selectRandomAnswer(),
                date: new Date().getTime(),
                type: "TO"
            });

            return { ...state, chats: array };

        default:
            return state;
    }
}

function selectRandomAnswer() {
    let answerArray = [
        "Сам такой",
        "Да я тоже",
        "Ну, если посмотреть на это с логической точки зрения, то не всё так плохо",
        "Сходи лучше за хлебом",
        "Давай поговорим о вечном",
        "Кажется, у меня отвалилась лодыжка",
        "Я хочу твоё мороженое!",
        "Всё, я спать",
        "Мне не о чем больше с тобой разговаривать",
        "Так сложилось исторически",
        "Всё бы ничего, но почему полотенце оранжевое?",
        "Уксус закончился!",
        "Закрой этот компьютер",
        "Я пью кофе",
        "Моя кошка меня не любит",
        "Сегодня довольно пасмурно, чтобы принимать подобные решения",
        "Давай бросим и уедем за горизонт!",
        "Бумажечка не найдётся?",
        "Эх-эх-эх... не повезло тебе",
        "Я тоже так думаю",
        "Закрой глаза и иди в другую сторону",
        "Мой дед это не одобряет",
        "А ты сам когда-нибудь читал что-нибудь подобное?",
        "Это не бывать!",
        "Это же моя сестра"
    ];
    return answerArray[Math.round((- 0.5 + Math.random() * (answerArray.length + 1)))];
}