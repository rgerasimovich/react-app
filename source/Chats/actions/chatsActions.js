import * as ConstantChats from '../constants/chatsConstants'

export function getChats() {

    return (dispatch, getState) => {
        dispatch({
            type: ConstantChats.GET_CHATS_REQUEST
        });
        let state = getState();
        if (state.chats.fistLoad) {
            dispatch({
                type: ConstantChats.GET_CHATS_SUCCESS
            })
        }
        else {
            fetch('/testApi/chats.json')
                .then(response => response.json().then(body => ({ response, body })))
                .then(({ response, body }) => {
                    if (response.ok) {
                        dispatch({
                            type: ConstantChats.GET_CHATS_SUCCESS,
                            chats: body.items
                        })
                    }
                });
        }
    }
}


export function getAllMessage(idUser) {
    return (dispatch, getState) => {
        dispatch({
            type: ConstantChats.GET_CHAT_REQUEST
        });
        let state = getState().chats;
        let usersChat = null;
        state.chats.forEach(item => {
            if (item.user.id === parseInt(idUser)) {
                usersChat = item;
            }
        });
        if (!usersChat) {

        }
        else {
            if (usersChat.loadAllMessage) {
                dispatch({
                    type: ConstantChats.GET_CHAT_SUCCESS
                })
            }
            else {
                fetch('/testApi/chat.json?idUser='+idUser)
                    .then(response => response.json().then(body => ({ response, body })))
                    .then(({ response, body }) => {
                        if (response.ok) {
                            dispatch({
                                type: ConstantChats.GET_CHAT_SUCCESS,
                                chat: body.message,
                                idUser: idUser
                            })
                        }
                    });
            }
        }
    }
}


export function sendMessage(idUser, message) {

    return (dispatch) => {
        dispatch({
            type: ConstantChats.SEND_MESSAGE_REQUEST
        });

        dispatch({
            type: ConstantChats.SEND_MESSAGE_SUCCESS,
            idUser: idUser,
            message: message
        });

        setTimeout(() => {
            dispatch({
                type: ConstantChats.SEND_ANSWER_SUCCESS,
                idUser: idUser
            });
        }, 1000);
    }
}