import React, { Component } from 'react'
import {Tabs, Tab} from 'material-ui/Tabs';
import { ChatEntity } from '../chatEntity/chatEntityComponent';
import { FriendEntity } from '../friendEntity/friendEntityComponent';

export class ChatsComponent extends Component {
    constructor(props) {
        super(props);
        this.props.getChats();
    }

    render() {
        const { chats, loading } = this.props;

        let chatsEntityArray = [];
        let friendsEntityArray = [];
        chats.forEach(item => {
           if (item.message.length) {
               chatsEntityArray.push(item);
           }
           else {
               friendsEntityArray.push(item);
           }
        });
        chatsEntityArray = chatsEntityArray.sort((a, b) => {
           if (a.message[a.message.length - 1].date > b.message[b.message.length - 1].date) {
               return -1;
           }
           if (a.message[a.message.length - 1].date < b.message[b.message.length - 1].date) {
               return 1;
           }
           return 0;
        });
        friendsEntityArray = friendsEntityArray.sort((a, b) => {
            if (a.user.name > b.user.name) {
                return 1;
            }
            if (a.user.name < b.user.name) {
                return -1;
            }
            return 0;
        });

        return (
            <Tabs>
                <Tab label="Диалоги">
                    <div>
                        {loading ?
                            <div>загрузка</div>
                            :
                            <div className="chats-list" data-flex data-layout="column" data-layout-align="start stretch">
                                {
                                    chatsEntityArray.map((entry, index) =>
                                        <ChatEntity chat={entry} key={index}/>
                                    )
                                }
                            </div>
                        }
                    </div>
                </Tab>
                <Tab label="Друзья">
                    <div>
                        {loading ?
                            <div>загрузка</div>
                            :
                            <div className="chats-list" data-flex data-layout="column" data-layout-align="start stretch">
                                {
                                    friendsEntityArray.map((entry, index) =>
                                        <FriendEntity chat={entry} key={index}/>
                                    )
                                }
                            </div>
                        }
                    </div>
                </Tab>
            </Tabs>
        )
    }
}