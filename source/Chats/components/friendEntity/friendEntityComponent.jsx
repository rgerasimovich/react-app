import React, { Component } from 'react'
import {Router, Link} from 'react-router'

export class FriendEntity extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        const { chat } = this.props;

        return (
            <Link to={'/chat/' + chat.user.id}>
                <div className="chat-entity chat-entity__friend">
                    <div className="chat-entity__name">{chat.user.name}</div>
                    <div className="chat-entity__last-message">начните диалог с этим пользователем</div>
                </div>
            </Link>
        )
    }
}