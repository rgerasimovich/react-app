import React, { Component } from 'react'
import {Router, Link} from 'react-router'

export class ChatEntity extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        const { chat } = this.props;
        let lastMessage = chat.message[chat.message.length-1];
        let dateLastMessage = moment(lastMessage.date);
        return (
            <Link to={'/chat/' + chat.user.id}>
                <div className="chat-entity" data-flex data-layout="column" data-layout-align="start stretch">
                    <div className="chat-entity__name">{chat.user.name}</div>
                    {
                        lastMessage.isFile ?
                            <div className="chat-entity__last-message">{dateLastMessage.format('DD.MM.YYYY в HH:mm:ss')}: отправлен файл</div>
                            :
                            <div className="chat-entity__last-message">{dateLastMessage.format('DD.MM.YYYY в HH:mm:ss')}: {lastMessage.text}</div>
                    }
                </div>
            </Link>
        )
    }
}