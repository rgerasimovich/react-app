import React, { Component } from 'react';
import { RouterApp } from './routerApp';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {
    blue500, blue700,
    pinkA200,
    grey100, grey300, grey400, grey500,
    white, darkBlack, fullBlack,
} from 'material-ui/styles/colors';
import  MuiThemeProvider  from 'material-ui/styles/MuiThemeProvider';

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: blue500,
        primary2Color: blue700,
        primary3Color: grey400,
        accent1Color: pinkA200,
        accent2Color: grey100,
        accent3Color: grey500,
        textColor: darkBlack,
        alternateTextColor: white,
        canvasColor: white,
        borderColor: grey300,
        pickerHeaderColor: blue500,
        shadowColor: fullBlack,
    }
});

export class AppConfig extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(muiTheme)}>
                 <RouterApp />
            </MuiThemeProvider>
        )
    }
}